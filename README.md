# Todify
Simple ToDo application.

## Getting started
### Prerequisites
* [Java jdk-8u201](https://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html)
* [Node.js 10.15.1](https://nodejs.org/en/download/)
* [Docker](https://www.docker.com/products/docker-desktop)

### Installation
Installing dependencies and devDependencies.
```sh
$ cd todify
$ npm install -d
```

### Building
Static files:
```sh
$ cd todify
$ npm run build
```

JAR:
```sh
$ cd todify
$ gradlew build
```

### Docker
Building Docker image:
```sh
$ cd todify
$ gradlew buildDocker
```

Running application:
```sh
$ cd todify
$ docker-compose up
```

Verify deployment:
http://localhost:8080