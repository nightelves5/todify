package com.sk.todify.service;

import com.sk.todify.domain.Todo;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.data.rest.core.annotation.RestResource;

import java.util.List;

public interface TodoRepository extends CrudRepository<Todo, Long> {

    @RestResource(path="byArchived")
    List findByArchived(@Param("archived") boolean archived);
}
