package com.sk.todify.domain;

import org.hibernate.annotations.ColumnDefault;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
@Table(name = "todo")
public class Todo {

    @Id
    @Basic(optional = false)
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    @Column(name = "TODO_ID")
    private Long todoId;

    @Column(name  = "DESCRIPTION")
    @NotNull
    private String todoDescription;

    @ColumnDefault(value="false")
    @Column(name = "ARCHIVED")
    private boolean archived;

    @ColumnDefault(value="false")
    @Column(name = "COMPLETED")
    private boolean completed;

    public Todo() {
    }

    public void setTodoId(Long todoId) {
        this.todoId = todoId;
    }

    public void setTodoDescription(String todoDescription) {
        this.todoDescription = todoDescription;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public void setCompleted(boolean completed) {
        this.completed = completed;
    }

    public Long getTodoId() {
        return todoId;
    }

    public String getTodoDescription() {
        return todoDescription;
    }

    public boolean isArchived() {
        return archived;
    }

    public boolean isCompleted() { return completed; }

}
