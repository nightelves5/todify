package com.sk.todify.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeController {

    @RequestMapping("/")
    public String index() {
        return "index.html";
    }

    @RequestMapping("/archive")
    public String archive() {
        return this.index();
    }

}
