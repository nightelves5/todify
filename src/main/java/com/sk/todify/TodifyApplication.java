package com.sk.todify;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TodifyApplication {

	public static void main(String[] args) {
		SpringApplication.run(TodifyApplication.class, args);
	}

}

