import React from 'react';
import ReactDOM from 'react-dom';
import Application from './application/App';

const { document } = window;

function render() {
  ReactDOM.render(<Application />, window.document.getElementById('react'));
}

document.addEventListener('DOMContentLoaded', render);
