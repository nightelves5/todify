import React from 'react';
import { Switch, Route } from 'react-router';
import * as routes from '../constant/routes';
import HomeLayout from '../component/view/Home';
import ArchiveLayout from '../component/view/Archive';

export default () => (
  <Switch>
    <Route exact path={routes.index()} component={HomeLayout} />
    <Route exact path={routes.archive()} component={ArchiveLayout} />
  </Switch>
);
