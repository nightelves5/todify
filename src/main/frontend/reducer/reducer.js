import * as actionType from '../constant/actionType';

export const initialState = { value: ' ' };

export const reducer = (state = initialState, action = {}) => {
  if (action.type === actionType.SET_VALUE) {
    const newState = Object.assign({}, state);
    newState.value = action.value;
    return newState;
  }
  return state;
};
