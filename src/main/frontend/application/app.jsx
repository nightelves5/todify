import React from 'react';
import { Provider } from 'react-redux';
import { withRouter } from 'react-router-dom';
import { ConnectedRouter } from 'connected-react-router';
import IndexLayout from '../component/layout';

import configureStore, { history } from '../store';

export default function Application() {
  const WrappedPage = withRouter(IndexLayout);
  const store = configureStore();

  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <WrappedPage />
      </ConnectedRouter>
    </Provider>
  );
}
