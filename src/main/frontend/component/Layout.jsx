import React from 'react';
import ApplicationRouter from '../router/routerSwitch';

export default function IndexLayout() {
  return (
    <div>
      <ApplicationRouter />
    </div>
  );
}
