import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import { Typography } from '@material-ui/core';

import CssBaseline from '@material-ui/core/CssBaseline';

import TodoList from '../element/TodoList';
import NavBar from '../element/common/NavBar';
import NavDrawer from '../element/common/NavDrawer';

const styles = theme => ({
  root: {
    display: 'flex',
  },
  appBarSpacer: theme.mixins.toolbar,
  content: {
    flexGrow: 1,
    padding: theme.spacing.unit * 3,
  },
});

// class HomeLayout extends React.Component {

export const ArchiveLayout = (props) => {
  const { classes } = props;

  return (
    <div className={classes.root}>
      <CssBaseline />
      <NavBar />

      <NavDrawer />

      <main className={classes.content}>
        <div className={classes.appBarSpacer} />

        <Typography variant="title" gutterBottom>
          List of Archived Tasks
        </Typography>

        <TodoList
          showArchived
        />
      </main>
    </div>
  );
};

export default withStyles(styles)(ArchiveLayout);
