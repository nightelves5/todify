import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  List, Paper, Typography, CircularProgress,
} from '@material-ui/core';

import TodoListItem from './TodoListItem';

import TodoListForm from './TodoListForm';

const styles = theme => ({
  root: {
    width: '95%',
    overflowX: 'auto',
    ...theme.mixins.gutters(),
    paddingTop: theme.spacing.unit * 2,
    paddingBottom: theme.spacing.unit * 2,
  },
  list: {
    overflow: 'hidden',
  },
  progress: {
    margin: theme.spacing.unit * 2,
  },
});

class TodoList extends React.Component {
  state = {
    todos: null,
    loading: true,
  };


  componentDidMount() {
    this.fetchResults();
  }

  fetchResults() {
    fetch(`/api/v1.0/todos/search/byArchived?archived=${this.props.showArchived}`)
      .then(res => res.json())
      .then(res => this.setState({ todos: res._embedded.todos, loading: false }));
  }

  render() {
    const { classes, showArchived } = this.props;
    const { todos, loading } = this.state;

    let data;

    if (loading) {
      data = <CircularProgress className={classes.progress} />;
    } else if (todos && todos.length > 0) {
      data = (
        <List className={classes.list}>
          {todos.map((todo, idx) => (
            <TodoListItem
              {...todo}
              key={todo.todoId}
              idx={todo.todoId}
              divider={idx !== todos - 1}
              showArchived={showArchived}
              fetchResults={this.fetchResults.bind(this)}
            />
          ))}
        </List>
      );
    } else {
      data = <Typography component="p">No Todo tasks!</Typography>;
    }

    return (
      <div>
        <TodoListForm
          onCreate={this.fetchResults.bind(this)}
        />

        <Paper className={classes.root}>
          { data }
        </Paper>

      </div>
    );
  }
}

export default withStyles(styles)(TodoList);
