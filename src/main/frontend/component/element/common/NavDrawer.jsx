import React from 'react';
import Drawer from '@material-ui/core/Drawer';

import { withStyles } from '@material-ui/core/styles';

import TaskListIcon from '@material-ui/icons/Assignment';
import ArchiveIcon from '@material-ui/icons/Folder';

import {
  List,
  ListItem,
  ListItemIcon,
  ListItemText,
} from '@material-ui/core';

const drawerWidth = 180;

const styles = theme => ({
  drawer: {
    width: drawerWidth,
    flexShrink: 0,
  },
  toolbar: theme.mixins.toolbar,
});

function ListItemLink(props) {
  return <ListItem button component="a" {...props} />;
}

export const NavDrawer = (props) => {
  const { classes } = props;

  return (
    <Drawer
      className={classes.drawer}
      variant="permanent"
      classes={{
        paper: classes.drawerPaper,
      }}
    >
      <div className={classes.toolbar} />
      <List>
        <ListItemLink href="/">
          <ListItemIcon>
            <TaskListIcon />
          </ListItemIcon>
          <ListItemText primary="Task List" />
        </ListItemLink>
        <ListItemLink href="/archive">
          <ListItemIcon>
            <ArchiveIcon />
          </ListItemIcon>
          <ListItemText primary="Archive" />
        </ListItemLink>
      </List>
    </Drawer>
  );
};


export default withStyles(styles)(NavDrawer);
