import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  AppBar,
  Toolbar,
  Typography,
} from '@material-ui/core';

const styles = theme => ({
  appBar: {
    zIndex: theme.zIndex.drawer + 1,
  },
});

export const NavBar = (props) => {
  const { classes } = props;

  return (
    <div>
      <AppBar
        position="absolute"
        className={classes.appBar}
      >
        <Toolbar>
          <Typography variant="title" color="inherit">
          Todify
          </Typography>
        </Toolbar>
      </AppBar>
    </div>
  );
};

export default withStyles(styles)(NavBar);
