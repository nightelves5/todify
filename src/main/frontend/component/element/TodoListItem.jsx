import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import {
  ListItem,
  Checkbox,
  IconButton,
  ListItemText,
  ListItemSecondaryAction,
} from '@material-ui/core';

import {
  ArchiveOutlined,
  UnarchiveOutlined,
  DeleteOutlined,
} from '@material-ui/icons';

const styles = {
  listText: {
    'word-break': 'break-all',
  },
};

class TodoListItem extends React.Component {
  setCompletedStatus() {
    const options = {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        completed: !this.props.completed,
      }),
    };

    fetch(`/api/v1.0/todos/${this.props.idx}`, options)
      .then(
        /* element is not updated yet */
        setTimeout(() => {
          this.props.fetchResults();
        }, 500),
      );
  }

  setArchiveStatus = (idx, archived) => {
    const options = {
      method: 'PATCH',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        archived,
      }),
    };

    fetch(`/api/v1.0/todos/${idx}`, options)
      .then(
        /* element is not updated yet */
        setTimeout(() => {
          this.props.fetchResults();
        }, 500),
      );
  }

  deleteTodo() {
    const options = {
      method: 'DELETE',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
    };

    fetch(`/api/v1.0/todos/${this.props.idx}`, options)
      .then(
        /* element is not updated yet */
        setTimeout(() => {
          this.props.fetchResults();
        }, 500),
      );
  }

  render() {
    const { classes, showArchived } = this.props;

    let primaryAction;
    let secondaryAction;

    if (showArchived) {
      /* Archive Page */
      primaryAction = (
        <IconButton onClick={() => this.setArchiveStatus(this.props.idx, !this.props.archived)}>
          <UnarchiveOutlined />
        </IconButton>
      );

      secondaryAction = (
        <IconButton onClick={() => this.deleteTodo(this.props.idx)}>
          <DeleteOutlined />
        </IconButton>
      );
    } else {
      /* Task List page */
      primaryAction = (
        <Checkbox
          onClick={() => this.setCompletedStatus(this.props.idx, !this.props.completed)}
          checked={this.props.completed}
          disableRipple
        />
      );

      secondaryAction = (
        <IconButton onClick={() => this.setArchiveStatus(this.props.idx, !this.props.archived)}>
          <ArchiveOutlined />
        </IconButton>
      );
    }

    return (
      <ListItem divider={this.props.divider}>
        {primaryAction}

        <ListItemText
          className={classes.listText}
          primary={this.props.todoDescription}
        />

        <ListItemSecondaryAction>
          {secondaryAction}
        </ListItemSecondaryAction>
      </ListItem>
    );
  }
}

export default withStyles(styles)(TodoListItem);
