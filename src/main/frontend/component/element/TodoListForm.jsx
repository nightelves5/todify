import React from 'react';

import { withStyles } from '@material-ui/core/styles';

import AddIcon from '@material-ui/icons/Add';

import {
  Dialog,
  DialogActions,
  DialogContent,
  DialogContentText,
  DialogTitle,
  TextField,
  Fab,
  Button,
} from '@material-ui/core';

const styles = theme => ({
  fab: {
    margin: theme.spacing.unit,
    top: 'auto',
    left: 'auto',
    position: 'fixed',
    bottom: theme.spacing.unit * 2,
    right: theme.spacing.unit * 2,
    'z-index': 900,
  },
});

class TodoListItem extends React.Component {
  state = {
    open: false,
  };

  handleDescription = (event) => {
    this.setState({ description: event.target.value });
  }

  handleSubmit = () => {
    const options = {
      method: 'POST',
      headers: {
        Accept: 'application/json',
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({
        todoDescription: this.state.description,
      }),
    };

    fetch('/api/v1.0/todos', options)
      .then(this.handleClose())
      .then(
        /* element is not added yet */
        setTimeout(() => {
          this.props.onCreate();
        }, 500),
      );
  };

  handleClickOpen = () => {
    this.setState({ open: true });
  };

  handleClose = () => {
    this.setState({ open: false });
  };

  render() {
    const { classes } = this.props;
    const { open } = this.state;

    return (
      <div>
        <Fab color="primary" aria-label="Add" className={classes.fab} onClick={this.handleClickOpen}>
          <AddIcon />
        </Fab>

        <Dialog
          open={open}
          onClose={this.handleClose}
          aria-labelledby="form-dialog-title"
        >
          <DialogTitle id="form-dialog-title">Create new ToDo task</DialogTitle>
          <DialogContent>
            <DialogContentText>
              Here you can create a new ToDo task.
            </DialogContentText>
            <TextField
              autoFocus
              margin="dense"
              id="desc"
              label="Description"
              type="text"
              fullWidth
              autoComplete="off"
              onChange={this.handleDescription.bind(this)}
            />
          </DialogContent>
          <DialogActions>
            <Button onClick={this.handleClose} color="primary">
              Cancel
            </Button>
            <Button onClick={this.handleSubmit} color="primary">
              Ok
            </Button>
          </DialogActions>
        </Dialog>

      </div>
    );
  }
}

export default withStyles(styles)(TodoListItem);
