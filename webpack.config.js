const path = require('path');

module.exports = {
    entry: path.join(__dirname, 'src', 'main', 'frontend', 'index.jsx'),
    devtool: 'sourcemaps',
    cache: true,
    mode: 'production',
	resolve: {
		extensions: ['.js', '.jsx'],
	},
    output: {
        path: path.join(__dirname, 'src', 'main', 'resources', 'static', 'build'),
        filename: 'bundle.js'
    },
    module: {
        rules: [
            {
                test: path.join(__dirname, '.'),
                exclude: /(node_modules)/,
                use: [{
                    loader: 'babel-loader',
                    options: {
                        presets: ["@babel/preset-env", "@babel/preset-react"],
						plugins: ["@babel/plugin-proposal-class-properties"],
                    }
                }]
            }
        ]
    }
};
