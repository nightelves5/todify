GRANT ALL PRIVILEGES ON DATABASE todify_db TO docker;

CREATE TABLE todo (
  todo_id     serial PRIMARY KEY,
  description varchar(120) not null,
  archived    boolean not null,
  completed   boolean not null
);
